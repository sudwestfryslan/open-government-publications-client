var project_location    = 'plugins/open-government-publications-cl';
var function_vers_var   = 'OPEN_GOVPUB_CL_VERSION';
var functions_file      = 'open-government-publications-cl.php';

require('es6-promise').polyfill();

var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    cleanCSS      = require('gulp-clean-css'),
    rtlcss        = require('gulp-rtlcss'),
    autoprefixer  = require('gulp-autoprefixer'),
    plumber       = require('gulp-plumber'),
    gutil         = require('gulp-util'),
    rename        = require('gulp-rename'),
    replace       = require('gulp-replace'),
    concat        = require('gulp-concat'),
    jshint        = require('gulp-jshint'),
    uglify        = require('gulp-uglify'),
    svgSprite     = require('gulp-svg-sprite'),
    imagemin      = require('gulp-imagemin'),
    browserSync   = require('browser-sync').create(),
    zip           = require('gulp-zip'),
    header        = require('gulp-header'),
    del           = require('del');

var svgConfig = {
  mode: {
    symbol: {
      inline: true,
      prefix: ".svg %s-svg",
    }
  },
  shape: {
    id: {
      generator: function(name, file) {
        var svg_id = 'svg-' + name;
        return svg_id.replace(/\.[^/.]+$/, "");
      }
    }
  }
};

var onError = function( err ) {
  console.log('An error occurred:', gutil.colors.magenta(err.message));
  gutil.beep();
  // this.emit('end');
};

// Grab the package.json file for the version
var getPackageJson = function () {
  var fs = require('fs');

  return JSON.parse(fs.readFileSync( 'package.json', 'utf8'));
};

// Sass
gulp.task('sass', function() {
  return gulp.src('./wp-content/' + project_location + '/assets/src/**/sass/style.scss')
  .pipe(plumber({ errorHandler: onError }))
  .pipe(sass())
  .pipe(autoprefixer())
  .pipe(rename(function(path)
  {
    path.dirname = path.dirname.replace('sass', 'css');
  }
  ))
  .pipe(cleanCSS())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('./wp-content/' + project_location + '/assets/dist'))
});

// Javascript Frontend
gulp.task('js-front', function() {
  return gulp.src([
    './wp-content/' + project_location + '/assets/src/js/inc/*.js',
    './wp-content/' + project_location + '/assets/src/js/base.js'
  ])
  .pipe(concat('base.js'))
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify())
  .pipe(gulp.dest('./wp-content/'+project_location+'/assets/dist/js'));
});

// JavaScript Admin
gulp.task('js-admin', function() {
  return gulp.src([
    './wp-content/' + project_location + '/assets/src/admin/js/inc/*.js',
    './wp-content/' + project_location + '/assets/src/admin/js/base.js'
  ])
  .pipe(concat('base.js'))
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify())
  .pipe(gulp.dest('./wp-content/'+project_location+'/assets/dist/admin/js'));
});

// Javascript libraries
gulp.task('js-lib', function() {
  return gulp.src(['./wp-content/' + project_location + '/assets/src/**/js/lib/*.js'])
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify())
  .pipe(gulp.dest('./wp-content/' + project_location + '/assets/dist'));
});

// Images
gulp.task('images', function() {
  return gulp.src('./wp-content/' + project_location + '/assets/src/**/images/*')
  .pipe(plumber({ errorHandler: onError }))
  .pipe(imagemin({ optimizationLevel: 7, progressive: true }))
  .pipe(gulp.dest('./wp-content/' + project_location + '/assets/dist'));
});

gulp.task('svg', function() {

  gulp.src('./wp-content/'+project_location+'/assets/svg/*.svg')
  .pipe(svgSprite(svgConfig))
  .pipe(gulp.dest('./wp-content/'+project_location+'/assets'));

});


gulp.task('font', function() {

  gulp.src('./wp-content/' + project_location + '/assets/src/**/fonts/*')
  .pipe(gulp.dest('./wp-content/' + project_location + '/assets/dist'));

});

// Watch
gulp.task('watch', function() {
  browserSync.init({
    files: ['./wp-content/' + project_location + '/**/*.*'],
    proxy: {
      target: '127.0.0.1',
      ws: true,
      proxyReq: [
        function(proxyReq) {
          proxyReq.setHeader('Access-Control-Allow-Origin', '*');
        }
      ]
    },
    snippetOptions: {
      whitelist: ['/wp-admin/admin-ajax.php'],
      blacklist: ['/wp-admin/**']
    }
  });
  gulp.watch('./wp-content/' + project_location + '/assets/src/**/sass/**/*.scss', ['sass']);
  gulp.watch('./wp-content/' + project_location + '/assets/src/**/js/**/*.js', ['js-lib', 'js-admin', 'js-front']);
  gulp.watch('./wp-content/' + project_location + '/assets/src/**/images/**/*', ['images']);
  gulp.watch('./wp-content/' + project_location + '/assets/src/**/svg/**/*', ['svg']);
});


// Set global variables
var newVer    = null;
var src_path  = 'wp-content/' + project_location;
var pkg       = getPackageJson();
var wp        = pkg.wordpress;

// Set versionnumber
gulp.task('clean-dist', function(){
  
  // Clean dist folder
  return del(['./dist/**', '!./dist']);

});

// Set versionnumber
gulp.task('update-start', function(){

  // get the version which we want to bump up to.
  var argv      = require('yargs').argv;
  
  var version   = argv.type;
  var noupdate  = argv.noupdate; // --noupdate
  
  if(typeof(argv.type) === 'undefined') {
    version = 'patch'; // major.minor.patch (https://semver.org/)
  }

  if( typeof(argv.noupdate) === 'undefined' ) { 
    noupdate = false;
  } else {
    noupdate = true;
  }

  // require some packages that we need
  var bump          = require('gulp-bump');
  var semver        = require('semver');
  
  if( noupdate ) {
    newVer = pkg.version;
  } else {
    // increment version
    newVer = semver.inc(pkg.version, version);
  }

  // files we want to transfer for release
  return gulp.src( 'package.json' )
  .pipe(bump({
    version: newVer,
    type:version
  }))
  .pipe(gulp.dest('./releases/' + newVer + '/' + pkg.name))
  .pipe(gulp.dest('./'));

});

gulp.task('update-style', function(){
  
  // Move the styles.css into the new folder
  // perform the version up in the style.css file
  return gulp.src( src_path + '/style.css', {'allowEmpty': true})
  .pipe(replace(/Version:[ ]+[0-9].+/g, 'Version: ' + newVer))
  .pipe(gulp.dest('./releases/'+newVer + '/'+pkg.name))
  .pipe(gulp.dest( src_path + '/' ));

});

gulp.task('update-functions', function(){
  
  var regex = new RegExp("\'" + function_vers_var + "\', \'+[0-9].+\'", "g");

  // Replace the functions file version
  return gulp.src( src_path + '/' + functions_file)
      .pipe(replace(/Version:[ ]+[0-9].+/g, 'Version: ' + newVer))
      .pipe(replace(/Requires at least:[ ]+[0-9].+/g, 'Requires at least: ' + wp.required))
      .pipe(replace(/Tested up to:[ ]+[0-9].+/g, 'Tested up to: ' + wp.tested))
      .pipe(replace(regex, '\'' + function_vers_var + '\', \'' + newVer + '\''))
      .pipe(gulp.dest('./releases/'+newVer + '/'+pkg.name))
      .pipe(gulp.dest( src_path + '/' ));

});

gulp.task('update-changelog', function(){
  
  return gulp.src('CHANGELOG.md')
  .pipe(header(
    "=== " + pkg.description + " ===\n\n" +
    "Requires at least: WordPress " + wp.required + "\n" +
    "Tested up to: WordPress " + wp.tested + "\n" +
    "Stable tag: " + newVer + "\n" +
    "Version: " + newVer + "\n\n" +
    "## Changelog\n\n"
  ))
  .pipe(rename("readme.txt"))
  .pipe(gulp.dest('./'));

});

// Our versioning
gulp.task('update-finish', function(){

  // move package files for deployment
  return gulp.src([
      src_path + '/screenshot.*',
      src_path + '/**/*.php',
      src_path + '/**/*.mo',
      src_path + '/**/*.css',
      src_path + '/**/*.js',
      src_path + '/**/*.svg',
      src_path + '/**/*.png',
      src_path + '/**/*.jpg',
      src_path + '/**/*.jpeg',
      src_path + '/**/*.gif',
      '!' + src_path + '/assets/src/*',
      '!' + src_path + '/functions.php',
    ], {base: './' + src_path}
  ).pipe(gulp.dest('./releases/' + newVer + '/'+pkg.name));

});

gulp.task('zip-release', function(){

  // zip the release
  return gulp.src('./releases/' + newVer + '/**').pipe(zip(newVer + '.zip')).pipe(gulp.dest('dist'));

});

// Set build task
gulp.task('build', gulp.series('svg', 'sass', 'images', 'font', 'js-lib', 'js-admin', 'js-front'));

// Our versioning
gulp.task('release', gulp.series(
  'clean-dist',
  'update-start',
  'update-style',
  'update-functions',
  'update-changelog',
  'update-finish',
  'zip-release'
));

// Set default tasks
gulp.task('default', gulp.series('build', 'watch'));