<div class="wrap">
	<h1><?php echo get_admin_page_title(); ?></h1>
	<hr class="wp-header-end">
	<form method="post" action="options.php">
		<?php
			settings_fields( 'open_govpub_cl' );
			do_settings_sections( 'open_govpub_cl' );
			submit_button();
		?>
	</form>
</div>