<?php
if( ! class_exists( 'openGovpubClService' ) ) {
    
    /**
     * openGovpub Service
     *
     * @since   1.0.0
     */
    class openGovpubClService {

    	// Set endpoint 
    	private $search_endpoint 	= null;
    	private $types_endpoint 		= null;
        
        /**
         * The single instance of the class.
         *
         * @var openGovpubClService|null
         */
        protected static $instance = null;
        
        /**
         * Gets the main openGovpubClService Instance.
         *
         * @static
         * 
         * @return openGovpubClService Main instance
         */
        public static function instance() {
            
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;

        }

        /**
         * openGovpubClService Constructor.
         */
        public function __construct()
        {
            
			// Set search endpoint
			if ( is_null( $this->search_endpoint ) )
				$this->search_endpoint = get_open_govpub_cl_setting('search_endpoint');

			// Set types endpoint
			if ( is_null( $this->types_endpoint ) )
				$this->types_endpoint = get_open_govpub_cl_setting('types_endpoint');

        }

        public function get_types()
        {

        	// Set the endpoint url
        	$url 		= $this->types_endpoint;

        	// Return the response
        	return $this->request($url);

        }

        public function get_search_results( $arguments  = array() )
        {

			// Add the arguments to the endpoint url
			$url 		= add_query_arg($arguments, $this->search_endpoint);

			// Get raw response and json decode to object
			$response 	= $this->request($url, true);
			$response 	= json_decode($response, false);

			// Return the response
			return $response;

        }

        /**
		 * Make the request
		 */
		public function request($url, $raw = false)
		{

			// Get cURL resource
			$curl = curl_init();

			// Set curl options
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER  => 1,
				CURLOPT_URL             => $url,
			));

			// Send the request & save response to $response
			$response = curl_exec($curl);


			// Close request to clear up some resources
			curl_close($curl);

			// Return the response
			return ($raw ? $response : json_decode($response, true));

		}

    }

}