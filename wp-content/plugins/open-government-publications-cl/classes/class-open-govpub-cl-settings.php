<?php
if( ! class_exists( 'openGovpubClSettings' ) ) {
    
    /**
     * openGovpub Initial setup
     *
     * @since   1.0.0
     */
    class openGovpubClSettings {
        
        /**
         * The single instance of the class.
         *
         * @var openGovpubClSettings|null
         */
        protected static $instance = null;
        
        /**
         * Gets the main openGovpubClSettings Instance.
         *
         * @static
         * 
         * @return openGovpubClSettings Main instance
         */
        public static function instance() {
            
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;

        }

        /**
         * openGovpubClSettings Constructor.
         */
        public function __construct()
        {
            
            // Settings item to the menu
        	add_action( 'admin_menu', array($this, 'add_settings_menu_item') );

            // Add settings
            add_action( 'admin_init', array($this, 'add_settings') );

        }

        public function add_settings_menu_item()
        {
			
			add_options_page(
				__('Open Government Publications', 'open-govpub-cl'),
				__('Open Publications', 'open-govpub-cl'),
				'manage_options',
				'open-govpub-cl-settings',
				array($this, 'show_settings_page')
			);
        
        }

        public function add_settings()
        {

        
            // Register the setting
            register_setting( 'open_govpub_cl', 'open_govpub_cl_settings' );

            // Add section
            add_settings_section(
                'open_govpub_cl_settings_section',
                __( 'Endpoints', 'open-govpub-cl' ),
                array($this, 'section_intro'),
                'open_govpub_cl'
            );

            // search_endpoint field
            add_settings_field(
                'search_endpoint',
                __( 'Search endpoint', 'open-govpub-cl' ),
                array($this, 'search_endpoint_field_render'),
                'open_govpub_cl',
                'open_govpub_cl_settings_section'
            );

            // types_endpoint field
            add_settings_field(
                'types_endpoint',
                __( 'Types endpoint', 'open-govpub-cl' ),
                array($this, 'types_endpoint_field_render'),
                'open_govpub_cl',
                'open_govpub_cl_settings_section'
            );

        }

        public function show_settings_page()
        {

        	// Include the settings page
        	include OPEN_GOVPUB_CL_DIR . '/views/admin/view-open-govpub-cl-settings.php';

        }

        public function section_intro()
        {

        }

        public function search_endpoint_field_render()
        {

        	// Set the name and value
			$name 	= 'open_govpub_cl_settings[search_endpoint]';
			$value 	= get_open_govpub_cl_setting('search_endpoint');

        	// Include the input
        	include OPEN_GOVPUB_CL_DIR . '/views/input/view-open-govpub-cl-text.php';

        }

        public function types_endpoint_field_render()
        {

        	// Set the name and value
			$name 	= 'open_govpub_cl_settings[types_endpoint]';
			$value 	= get_open_govpub_cl_setting('types_endpoint');

        	// Include the input
        	include OPEN_GOVPUB_CL_DIR . '/views/input/view-open-govpub-cl-text.php';

        }

    }
    new openGovpubClSettings();

}