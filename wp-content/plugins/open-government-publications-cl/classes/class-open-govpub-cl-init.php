<?php
if( ! class_exists( 'openGovpubClInit' ) ) {
    
    /**
     * openGovpub Initial setup
     *
     * @since   1.0.0
     */
    class openGovpubClInit {
        
        /**
         * The single instance of the class.
         *
         * @var openGovpubClInit|null
         */
        protected static $instance = null;
        
        /**
         * Gets the main openGovpubClInit Instance.
         *
         * @static
         * 
         * @return openGovpubClInit Main instance
         */
        public static function instance() {
            
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;

        }

        /**
         * openGovpubClInit Constructor.
         */
        public function __construct()
        {
            
            // Enqueue styles and scripts
            add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts') );

        }

        public function enqueue_scripts() 
        {

            $load_css = apply_filters('load_open_govpub_cl_styles', true);

            if( $load_css ) {
            
                // Enqueue plugin style
                wp_enqueue_style( 
                    'open_govpub_cl', 
                    OPEN_GOVPUB_CL_URL . '/assets/dist/css/style.min.css',
                    array(), 
                    OPEN_GOVPUB_CL_VERSION 
                );

            }

        }

    }
    new openGovpubClInit();

}