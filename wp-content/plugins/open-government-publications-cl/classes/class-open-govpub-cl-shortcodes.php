<?php
if( ! class_exists( 'openGovpubClShortcodes' ) ) {
    
    /**
     * openGovpub Shortcodes setup
     *
     * @since   1.0.0
     */
    class openGovpubClShortcodes {
        
        /**
         * The single instance of the class.
         *
         * @var openGovpubClShortcodes|null
         */
        protected static $instance = null;
        
        /**
         * Gets the main openGovpubClShortcodes Instance.
         *
         * @static
         * 
         * @return openGovpubClShortcodes Main instance
         */
        public static function instance() {
            
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;

        }

        /**
         * openGovpubClShortcodes Constructor.
         */
        public function __construct()
        {
            
            // Add the shortcode
            add_shortcode( 'open_govpub_form', array($this, 'render_open_govpub_form') );

        }

        public function get_pagination_html( $pagination )
        {

        	ob_start();

        	// Get the variables
			$variables = array(
				'pagination' => $pagination
			);

        	// Include the pagination template
			get_open_govpub_cl_template('pagination.php', $variables);

			// Return the content
			return ob_get_clean();


        }

		public function get_types( $service ) {

			// Get all types
			$types 		= $service->get_types();
			$types 		= (is_array($types) ? $types : array());

			// Set default type
			$default 	= array('default' => __('Alle overheidspublicaties'));

			// Return the combined results
			return $default+$types;

		}

		public function get_search_results( $service, $atts = array() )
		{

			// Get search arguments
			$arguments 	= $this->get_search_arguments( $atts );

			// Get the search results
			$results 	= $service->get_search_results( $arguments );

			return array(
				'pagination' => array(
					'raw' 	=> $results->pagination,
					'html'	=> $this->get_pagination_html( $results->pagination )
				),
				'results' 	=> $results->data
			);

		}

		public function get_search_arguments( $atts )
		{

			// Get posts per page value
			$posts_per_page = (isset($atts['posts_per_page']) ? $atts['posts_per_page'] : 20);
			
			// Get order variables
			$orderby 		= get_open_govpub_cl_url_par('sortby');
			$order 			= get_open_govpub_cl_url_par('order', false);

			if( $orderby == 'post_title' && !$order ) {
				$order = 'ASC';
			} else {
				$order 			= ($order == 'ASC' ? 'ASC' : 'DESC');
			}

			// Return the arguments
			$arguments = array(
				's' 				=> get_open_govpub_cl_url_par('keyword'),
				'open_govpub_type' 	=> get_open_govpub_cl_url_par('type'),
				'limit'				=> $posts_per_page,
				'page'				=> get_open_govpub_cl_url_par('pub-page'),
				'orderby'			=> get_open_govpub_cl_url_par('sortby'),
				'order'				=> $order,
			);

			// Remove empty items
			return array_filter($arguments);

		}

		public function get_form_data( $atts ) {

			// Get the service instance
			$service = openGovpubClService::instance();

			$results = $this->get_search_results( $service, $atts );

			// Return the data
			return array(
				'types' 			=> $this->get_types( $service ),
				'results' 			=> $results['results'],
				'pagination' 		=> $results['pagination']['raw'],
				'pagination_html' 	=> $results['pagination']['html'],
				'c_type' 			=> get_open_govpub_cl_url_par('type', false),
				'c_order' 			=> get_open_govpub_cl_url_par('sortby', 'date'),
			);

		}

		public function render_open_govpub_form( $atts ) {
			
			// Get the attributes
			$atts = shortcode_atts( array(
				'posts_per_page' => 20
			), $atts, 'open_govpub_form' );

			ob_start();

			// Get the variables
			$variables = $this->get_form_data( $atts );

			// Include the view
			get_open_govpub_cl_template('search-form.php', $variables);

			// Return the content
			return ob_get_clean();

		}

    }
    new openGovpubClShortcodes();

}