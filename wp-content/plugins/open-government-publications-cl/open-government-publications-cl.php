<?php
/**
 * Plugin Name: Open Government Publications - Client
 * Description: With this plugin you can connect to the Open Government Publications API
 * Version: 1.0.2
 * Author: Súdwest-Fryslân
 * Author URI: https://sudwestfryslan.nl/
 * Requires at least: 4.8
 * Tested up to: 5.2.2
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! defined('OPEN_GOVPUB_CL_VERSION')) define('OPEN_GOVPUB_CL_VERSION', '1.0.2');
if ( ! defined('OPEN_GOVPUB_CL_DEBUG')) define('OPEN_GOVPUB_CL_DEBUG', false);

if ( ! defined('OPEN_GOVPUB_CL_FILE')) define('OPEN_GOVPUB_CL_FILE', __FILE__);
if ( ! defined('OPEN_GOVPUB_CL_DIR')) define('OPEN_GOVPUB_CL_DIR', dirname(__FILE__));
if ( ! defined('OPEN_GOVPUB_CL_BASENAME')) define('OPEN_GOVPUB_CL_BASENAME', basename(OPEN_GOVPUB_CL_DIR));
if ( ! defined('OPEN_GOVPUB_CL_URL')) define('OPEN_GOVPUB_CL_URL', plugins_url(OPEN_GOVPUB_CL_BASENAME));

/**
 * Main Plugin Class
 *
 * @class openGovpubCl
 * @version  1.0.0
 */
class openGovpubCl {

	public function __construct() {

		// Load the language
		load_plugin_textdomain(
			'open-govpub',
			false,
			OPEN_GOVPUB_CL_BASENAME . '/languages'
		);

		// Include the updater class
		include OPEN_GOVPUB_CL_DIR . '/classes/class-open-govpub-cl-updater.php';

		// Include the helper functions
		include OPEN_GOVPUB_CL_DIR . '/lib/helpers.php';

		// Include the init class
		include OPEN_GOVPUB_CL_DIR . '/classes/class-open-govpub-cl-init.php';

		// Include the settings class
		include OPEN_GOVPUB_CL_DIR . '/classes/class-open-govpub-cl-settings.php';

		// Include the service class
		include OPEN_GOVPUB_CL_DIR . '/classes/class-open-govpub-cl-service.php';

		// Include the shortcodes class
		include OPEN_GOVPUB_CL_DIR . '/classes/class-open-govpub-cl-shortcodes.php';

	}

}
$GLOBALS['open-govpub-cl'] = new openGovpubCl();