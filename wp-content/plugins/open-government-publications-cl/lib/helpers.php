<?php
	function get_open_govpub_cl_setting( $name, $default = '' ) {

		 // Get settings
        $settings   = get_option( 'open_govpub_cl_settings' );

        // Return setting
        return (isset($settings[$name]) ? $settings[$name] : $default);

	}

	function get_open_govpub_cl_url_par( $name, $default = '' ) {

		// Get the value
		$value = (isset($_GET[$name]) && $_GET[$name] !== '' ? $_GET[$name] : $default);

		// Return sanitized value
		return sanitize_text_field($value);

	}

	function get_open_govpub_cl_css_class( $value_1, $value_2, $class = 'active' ) {

		// Return the class if the values match
		return ($value_1 == $value_2 ? $class : '');

	}

	function the_open_govpub_cl_css_class( $value_1, $value_2, $class = 'active' ) {

		// Echo the response
		echo get_open_govpub_cl_css_class( $value_1, $value_2, $class );

	}

	function get_open_govpub_cl_num_link( $number ) {

		// Return the link
		return add_query_arg( array('pub-page' => absint($number)) );

	}

	function get_open_govpub_cl_previous_page_link( $label = null ) {

		// Get current page number
		$c_page = get_open_govpub_cl_url_par('pub-page', 1);

		// Check if previous link is possible
		if( $c_page > 1 ) {

			if ( null === $label ) {
				$label = __( '&laquo; Previous Page' );
			}

			// Get the link
			$link = get_open_govpub_cl_num_link($c_page - 1);

			// Get the attributes
			$attr = apply_filters( 'previous_posts_link_attributes', '' );

			// Filter label
			$label = preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label );

			return '<a href="' . $link . "\" $attr>" . $label . '</a>';

		}

		// Return false as default
		return false;

	}

	function get_open_govpub_cl_next_page_link( $label = null, $max_page ) {

		// Get current page number
		$c_page 	= get_open_govpub_cl_url_par('pub-page', 1);
		$nextpage 	= $c_page+1;

		// Check if next link is possible
		if( $nextpage <= $max_page ) {

			if ( null === $label ) {
				$label = __( 'Next Page &raquo;' );
			}

			// Get the link
			$link = get_open_govpub_cl_num_link($nextpage);

			// Get the attributes
			$attr = apply_filters( 'next_posts_link_attributes', '' );

			// Filter label
			$label = preg_replace( '/&([^#])(?![a-z]{1,8};)/i', '&#038;$1', $label );

			return '<a href="' . $link . "\" $attr>" . $label . '</a>';

		}

		// Return false as default
		return false;

	}

	function get_open_govpub_cl_item_desc( $item, $combine = ' | ' ) {

		// Set publisher values
		$publisher = array(
			ucfirst($item->meta->organisationtype),
			$item->meta->publicationname
		);

		// Remove empty results
		$publisher 	= array_filter($publisher);

		// Format the date
		$date 		= date('d-m-Y', strtotime($item->created_at));

		$rules = array(
			$date,
			(!empty($publisher) ? 'Gepubliceerd door: ' . implode(', ', $publisher) : ''),
			$item->meta->betreft
		);

		// Remove empty rules
		$rules = array_filter($rules);

		return implode($combine, $rules);

	}

	function get_open_govpub_cl_template( $file, $variables = array() ) {

		$template_folder 	= 'open_govpub_cl';

		// Get the theme dir
		$theme_dir 			= get_template_directory();
		$theme_path 		= $theme_dir . '/' . $template_folder . '/' . $file;
		
		// Extract the variables
		extract($variables);

		// Check if file exists in theme
		if( file_exists($theme_path) ) {
			
			// Include the file from the template directory
			include $theme_path;

		} else {

			// Include the default file
			include OPEN_GOVPUB_CL_DIR . '/templates/' . $file;

		}

	}