<?php
	/** Stop execution if there's only 1 page */
    if( $pagination->max_num_pages <= 1 )
        return;

    $paged = $pagination->paged ? absint( $pagination->paged ) : 1;
    $max   = intval( $pagination->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="open-govpub--pagination"><ul>' . "\n";
 
    /** Previous Link */
    if ( get_open_govpub_cl_previous_page_link() )
        printf( '<li>%s</li>' . "\n", get_open_govpub_cl_previous_page_link() );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
    
        $pagenum = esc_url( get_open_govpub_cl_num_link( 1 ) );

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, $pagenum, '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_open_govpub_cl_num_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_open_govpub_cl_num_link( $max ) ), $max );
    }
 
    /** Next Link */
    if ( get_open_govpub_cl_next_page_link( null, $pagination->max_num_pages) )
        printf( '<li>%s</li>' . "\n", get_open_govpub_cl_next_page_link(null, $pagination->max_num_pages) );
 
    echo '</ul></div>' . "\n";