<div class="open-govpub--item">
	<a href="<?php echo $result->permalink; ?>" target="_blank"><?php echo $result->post_title; ?></a>
	<span class="item--link"><?php echo $result->permalink; ?></span>
	<div class="description">
		<p>
			<?php echo get_open_govpub_cl_item_desc( $result ); ?>
		</p>
	</div>
</div>