<div class="open-govpub--wrap">
	
	<form action="<?php the_permalink(); ?>" method="GET">
	
		<div class="search--bar">
			<input type="text" name="keyword" value="<?php echo get_open_govpub_cl_url_par('keyword'); ?>">
			<button type="submit">Zoeken</button>
		</div>
		<div class="search--wrap">
			<div class="search--results">
				<h2>Resultaten (<?php echo $pagination->found_posts; ?>)</h2>
				<div class="sort--options">
					<label>Sorteer op:</label>
					<select name="sortby" onchange="this.form.submit()">
						<option value="date" <?php selected($c_order, 'date', true); ?>>Meest recent</option>
						<option value="none" <?php selected($c_order, 'none', true); ?>>Relevantie</option>
						<option value="post_title" <?php selected($c_order, 'post_title', true); ?>>Titel</option>
					</select>
				</div>
				<?php
					if( !empty($results) ) {

						foreach ($results as $org_id => $result) {
							
							// Include the result item template
							get_open_govpub_cl_template('item.php', array('result' => $result));

						}

					} else {

						// Include the no results template
						get_open_govpub_cl_template('no-results.php');

					}
				?>
			</div>
			<div class="search--filters">
				<h2>Onderdelen</h2>
				<ul>
					<?php 
						foreach ($types as $slug => $title) {
							// Set need variables
							$slug 	= ($slug == 'default' ? false : $slug);
							$class 	= get_open_govpub_cl_css_class($c_type, $slug);
						?>
						<li>
							<a href="<?php echo add_query_arg( array(
								'type' 		=> $slug,
								'pub-page'	=> false
								)); ?>" class="<?php echo $class; ?>">
								<?php echo $title; ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>

	</form>

	<?php echo $pagination_html; ?>
</div>