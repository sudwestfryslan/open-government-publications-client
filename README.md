# Open Government Publications - Client (WP)

Information about developing is descripted below. 

## Overview

  * [Required](#markdown-header-required)
  * [Installation](#markdown-header-installation)
  * [Docker Commands](#markdown-header-docker-commands)
  * [Changelog](#markdown-header-changelog)
  
  
## Required
To develop on this project, you will need to download Docker: [Docker](https://www.docker.com/community-edition).
After the setup of Docker you can start with the [installation](##markdown-header-installatie) of the test environment.

## Installation
1. Download the most recent version of the project
2. Open the Terminal(Mac OS) or Powershell(Windows)
3. Navigate to the folder where you extracted the project
4. Run the next commamnd to start and build all the containers: `docker-compose up --build`
5. Go to the browser and navigate to [localhost](http://localhost/) (127.0.0.1)

## Docker Commands
Start the docker environment and (re)build all the container
`docker-compose up --build`

Start the docker environment
`docker-compose up`

Deleting the docker environment (dont't forget to delete the tmp folder if you wan't to reset de database)
`docker-compose rm -v`

## Changelog
Changes within this project and project releases can be logged in the [CHANGELOG.md](CHANGELOG.md).